 const express = require('express')
 const morgan = require('morgan')
const mongoose = require('mongoose')

const app = express()

//all app use
app.use(morgan('dev'))
app.use(express.urlencoded({extended:true}))
app.use(express.json())
app.set('view engine' , 'ejs')

app.get('/about',(req,res)=>{//about
    res.render('pages/about')
})

app.get('/help',(req,res)=>{//help
    res.render('pages/help')
})

//database test

// let Schema = mongoose.Schema //schema store in a variable

// let TestSchema = new Schema({ //mongodb database
//     name:String
// })

// let Testsdf  = mongoose.model('sajib',TestSchema) //mongodb model name


//database test














app.get('/', (req, res) => {

    let test = new Testsdf({
        name:'sajib saha'
    })

    test.save().then(respon=>{
        console.log(respon)
    }).catch(error=>console.log(error))

    let post = {
        title:'React native',
        Description:'Many different kinds of people use React Native: from advanced iOS developers to React beginners, to people getting started programming for the first time in their career. These docs were written for all learners, no matter their experience level or background.',
        publish:false
    }

    let data = [
        {
            title:'React native one',
            subtitle:'this is react native subtitle one'
        },
        {
            title:'React native two',
            subtitle:'this is react native subtitle two'
        },
        {
            title:'React native three',
            subtitle:'this is react native subtitle three'
        },
        {
            title:'React native four',
            subtitle:'this is react native subtitle four'
        },
        {
            title:'React native five',
            subtitle:'this is react native subtitle five'
        },
    ] 
    res.render('pages/index',{title:'React Native function Test',post,data})
})

app.get('*', (req, res) => {
    res.send('<h1>404 Error page Not Found</h1')
})

const PORT = process.env.PORT || 8080 

mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true}).then((res)=>{
    console.log('database connect')
    app.listen( PORT,()=>{
        console.log(`sarver is running on port ${PORT}`)
    })
}).catch(error=>{
    console.log(error)
})

